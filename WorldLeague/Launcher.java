package WorldLeague;

import Teams.NationalVolleyballTeam;
import Teams.Team;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Launcher {

    public static void main(String[] args) {
        ArrayList<NationalVolleyballTeam> teamsList = new ArrayList();
        
        IntercontinentalRound round = new IntercontinentalRound(6);
        
        teamsList = round.loadTeams();
        
        Map groupMap = new HashMap();
        
        groupMap = round.generateGroups(teamsList);
        
        round.displayStandings(groupMap);
        
    }
    
}
