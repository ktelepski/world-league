/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Teams;


public class NationalVolleyballTeam extends Team{
    
    public NationalVolleyballTeam(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    public int getPoints() {
        return points;
    }
    public int getGamesWon() {
        return gamesWon;
    }
    public int getGamesLost() {
        return gamesLost;
    }
    public int getSetsWon() {
        return setsWon;
    }
    public int getSetsLost() {
        return setsLost;
    }
    
    @Override
    public void updateTable(boolean gameWon, boolean tieBreak, int setsWon, int setsLost) {
        if (gameWon == true) {
            if (tieBreak == true) {               
                this.points = this.points + 2;
            } 
            else {
                this.points = this.points + 3;
            }
            this.gamesWon++;
        }
        else {
            if (tieBreak == true) {               
                this.points = this.points + 1;
            } 
            this.gamesLost++;
        }
        this.setsWon = this.setsWon + setsWon;
        this.setsLost = this.setsLost + setsLost;
    }

    @Override
    public String toString() {
        return name + ":  \t points=" + points
                + ", gamesWon=" + gamesWon + ", gamesLost=" + gamesLost
                + ", setsWon=" + setsWon + ", setsLost=" + setsLost + '}';
    }
    
    
    
    private String name;
    private int points = 0;
    private int gamesWon = 0;
    private int gamesLost = 0;
    private int setsWon = 0;
    private int setsLost = 0;
}
