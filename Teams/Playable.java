package Teams;


public interface Playable {

    public <T> void playGame(T team1, T team2);
}
