package WorldLeague;


import Teams.Playable;
import Teams.Team;
import java.util.List;
import java.util.Map;

public abstract class Round implements Playable {
    
    abstract void playGame(Team team1, Team team2);
    
    abstract <T> Map generateGroups(List<T> teamsList);
    
    abstract <T> void displayStandings(List<T> teamsList);
    
    abstract <T> List<T> loadTeams();

}
