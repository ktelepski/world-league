/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package WorldLeague;

import Teams.NationalVolleyballTeam;
import java.util.List;
import java.util.Random;
import Teams.Team;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class IntercontinentalRound extends Round {

    public IntercontinentalRound(int numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }
    
    @Override
    public  ArrayList<NationalVolleyballTeam> loadTeams() {
        ArrayList<NationalVolleyballTeam> teamsList = new ArrayList();
        
        NationalVolleyballTeam france = new NationalVolleyballTeam("France");
        teamsList.add(france);
        NationalVolleyballTeam poland = new NationalVolleyballTeam("Poland");
        teamsList.add(poland);
        NationalVolleyballTeam italy = new NationalVolleyballTeam("Italy");
        teamsList.add(italy);
        NationalVolleyballTeam russia = new NationalVolleyballTeam("Russia");
        teamsList.add(russia);
        NationalVolleyballTeam china = new NationalVolleyballTeam("China");
        teamsList.add(china);
        NationalVolleyballTeam usa = new NationalVolleyballTeam("USA");
        teamsList.add(usa);
        NationalVolleyballTeam germany = new NationalVolleyballTeam("Germany");
        teamsList.add(germany);
        NationalVolleyballTeam turkey = new NationalVolleyballTeam("Turkey");
        teamsList.add(turkey);
        NationalVolleyballTeam spain = new NationalVolleyballTeam("Spain");
        teamsList.add(spain);
        NationalVolleyballTeam romania = new NationalVolleyballTeam("Romania");
        teamsList.add(romania);
        NationalVolleyballTeam greece = new NationalVolleyballTeam("Greece");
        teamsList.add(greece);
        NationalVolleyballTeam latvia = new NationalVolleyballTeam("Latvia");
        teamsList.add(latvia);
        
        return teamsList;
    }
    
    public <T> Map generateGroups(List<T> teamsList) {
        Map groupMap = new HashMap();
        if (teamsList.size() == 8) {            
            
            List<T> groupA = new ArrayList();
            List<T> groupB = new ArrayList();
            
            groupA = (List<T>) createGroup(teamsList);
            groupMap.put("A", groupA);            
            groupB = (List<T>) createGroup(teamsList);
            groupMap.put("B", groupB);           
        }
        
        if (teamsList.size() == 12) {
            List<T> groupA = new ArrayList();
            List<T> groupB = new ArrayList();
            List<T> groupC = new ArrayList();
            
            groupA = (List<T>) createGroup(teamsList);
            groupMap.put("A", groupA);
            groupB = (List<T>) createGroup(teamsList);
            groupMap.put("B", groupB);
            groupC = (List<T>) createGroup(teamsList);
            groupMap.put("C", groupC);
        }
        
        if (teamsList.size() == 16) {
            List<T> groupA = new ArrayList();
            List<T> groupB = new ArrayList();
            List<T> groupC = new ArrayList();
            List<T> groupD = new ArrayList();
            
            groupA = (List<T>) createGroup(teamsList);
            groupMap.put("A", groupA);
            groupB = (List<T>) createGroup(teamsList);
            groupMap.put("B", groupB);
            groupC = (List<T>) createGroup(teamsList);
            groupMap.put("C", groupC);
            groupD = (List<T>) createGroup(teamsList);
            groupMap.put("D", groupD);
        }
        
        return groupMap;
    }
    
    private <T> List<T> createGroup(List<T> teamsList) {
        Random generator = new Random();
        List<T> group = new ArrayList();        
        int index;
        
        //choose randomly 4 teams to the group
        for(int i = 0; i < 4; i++) {
            //randomly generate numbers ranging from 0 to listSize
            index = generator.nextInt(teamsList.size()); 
            T team = teamsList.get(index);            
            teamsList.remove(index); //remove added team to avoid doubling
            group.add(team);
        }
        return group;
    }

    
    @Override
    public void playGame(Team team1, Team team2) {
        int setsNumberT1 = 0;
        int setsNumberT2 = 0;
        boolean tieBreak = false;
        Random generator = new Random();
        
        //game simulation, set by set
        for(int i = 0; setsNumberT1 != 3 && setsNumberT2 != 3; i++) {
            if (generator.nextBoolean() == true) {
                setsNumberT1++;
            } else {
                setsNumberT2++;
            }
        }
        //check if tie break
        if (setsNumberT1 + setsNumberT2 == 5) {
            tieBreak = true;
        }
        System.out.println("End of the Game");
        System.out.println(team1.getName() + " " + setsNumberT1 + ":" + setsNumberT2 + " " + team2.getName());
        
        //update team standings
        if (setsNumberT1 == 3) {
            team1.updateTable(true, tieBreak, setsNumberT1, setsNumberT2);
            team2.updateTable(false, tieBreak, setsNumberT2, setsNumberT1);
        } else {
            team1.updateTable(false, tieBreak, setsNumberT1, setsNumberT2);
            team2.updateTable(true, tieBreak, setsNumberT2, setsNumberT1);
        }
    }

    @Override
    public <T> void playGame(T team1, T team2) {
        
    }

    @Override
    public <T> void displayStandings(List<T> teamsList) {
        for (int i = 0; i < teamsList.size(); i++)
        {
            System.out.println(teamsList.get(i));
        }
    }
    
    public <T> void displayStandings(Map groupMap) {
        List<T> group = new ArrayList();
        String mapIndex = "A";
        
        for(int i = 0; i < groupMap.size(); i++) {
            
            group = (List<T>) groupMap.get(mapIndex);
            System.out.println("\nGroup " + mapIndex + ":");
            displayStandings(group);
            char buf = mapIndex.charAt(0);
            buf = (char) (buf + 1);
            mapIndex = Character.toString(buf);
            
        }   
    }
    
    private int numberOfTeams;

}
